
var Commands = function(Terminal) {

  Terminal.addCommand('clear', function () {
    Terminal.clear();
    return;
  });

        case 'start':
          Terminal.output('<p>~# DEADSWAP ACTIVATED @~~/~</p>');
          output('stop to deactivate</br>');
          output('monitor to watch activity');
          break;
        case 'monitor':
          output('ctrl+c to stop monitoring');
          break;
        case 'stop':
          output('<p>~# DEADSWAP DEACTIVATED @~~/~</p>');
          output('start to reactivate');
          break;
        case 'date':
          output((new Date()).toLocaleString());
          break;
        case 'help':
          output('<div class="ls-files">' + CMDS_.join('<br>') + '</div>');
          break;
        case 'version':
        case 'ver':
          output(VERSION_);
          break;
        case 'about':
          output(document.title +
                 ' - By: Telekommunisten &lt;deadSwap@telekommunisten.net&gt;');
          break;
        default:
          if (cmd) {
            output(cmd + ': command not found');
          }
      };

      this.value = ''; // Clear/setup line for next input.
    }
  }

  function clear_(input) {
    output_.innerHTML = '';
    input.value = '';
    document.documentElement.style.height = '100%';
    interlace_.style.height = '100%';
  }

  function output(html) {
    output_.insertAdjacentHTML('beforeEnd', html);
    output_.scrollIntoView();
    cmdLine_.scrollIntoView();
  }

  return {
    init: function() {
      output('<div>Welcome to ' + document.title +
             '! (v' + VERSION_ + ')</div>');
      output((new Date()).toLocaleString());
      output('<p>Documentation: type "help"</p>');

    },
    output: output,
    getCmdLine: function() { return cmdLine_; }
  }
};

