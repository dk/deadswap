/**************************************************
*
* deadSwap: The clandestine offline file sharing system
* Dmytri Kleiner <dk@telekommunisten.net>
*
* http://deadswap.net
*
* This program is free software.
* It comes without any warranty, to the extent permitted by
* applicable law. You can redistribute it and/or modify it under the
* terms of the Do What The Fuck You Want To Public License v2.
* See http://wtfpl.net for more details.
*
* The deadSwap UI uses styles, images and code examples
* taken from Eric Bidelman's HTML5 Terminal demo
* http://www.htmlfivewow.com/demos/terminal/terminal.html
*
* deadSwap is a miscommunication technology by Telekommunisten
*
***********************************/

/* jshint indent:2, browser: true, enforceall: true, nocomma: false, forin: false */
/* global SMS, localforage, cordova */

(function () {
  "use strict";

  localforage.getItem('state', function (err, storage) {
    var VERSION = '2.0';

    if (err) { write([ err ]); }

    var terminal = document.getElementById('terminal');
    var output = terminal.querySelector('output');
    var input = terminal.querySelector('#input-line .cmdline');

    var write = function (strings) {
      output.insertAdjacentHTML('beforeEnd', strings.join(''));
    };

    var search = [];
    var spos = 0;
    var stemp = '';

    document.addEventListener("deviceready", function () {
      cordova.plugins.notification.badge.configure({
        smallIcon: 'icon24',
        title: '%d messages processed'
      });

      document.addEventListener("backbutton", function() {
        // ignore backbutton
      }, false);
    }, false);

    var help = function () {
      var html = '';
      for (var key in commands) {
        if (typeof commands[key].help === 'string') {
          html = html + '<tr><td><strong>' + key + '</strong>&nbsp;</td><td>' + commands[key].help + '</td></tr>';
        }
      }
      write([ '<table>', html, '</table>' ]);
    };

    var commands = {
      help: { command: function () { help(); }}
    };

    var addCommand = function (name, help, command) {
      commands[name] = { help: help, command: command };
    };

    var run = function (cmd, args) {
      if (typeof commands[cmd] === 'object') {
        args = args || [];
        commands[cmd].command(args);
      } else if (cmd) {
        write([ cmd, ' command not found<br>' ]);
      }
    };

    document.addEventListener('click', function () {
      input.focus();
    }, false);

    input.addEventListener('keydown', function (e) {

      var entry = this.value.trim();

      if (e.keyCode === 13) { // enter

        var line = this.parentNode.parentNode.cloneNode(true);
        line.removeAttribute('id');
        line.classList.add('line');
        var echo = line.querySelector('input.cmdline');
        echo.autofocus = false;
        echo.readOnly = true;
        output.appendChild(line);
        stemp = '';

        var cmd;
        var args;
        if (entry) {
          args = entry.split(' ');
          cmd = args[0].toLowerCase();
          args = args.splice(1);
        }

        run(cmd, args);
        this.value = '';

      } else if (e.keyCode === 9) {

        var L;
        e.preventDefault();
        if (stemp === null) { stemp = entry; }
        L = stemp.length;
        if (search.length > 1) {
          spos = spos + 1;
          if (spos >= search.length) { spos = 0; }
        } else {
          var key;
          for (key in commands) {
            if (typeof commands[key].command === 'function') {
              if (key.length >= L && key.substr(0, L) === stemp) {
                search.push(key);
              }
            }
          }
        }
        if (search.length > 0) { this.value = search[spos]; }

      } else {
        spos = 0;
        search = [];
        if (e.keyCode > 47 && e.keyCode < 91) {
          stemp = stemp + String.fromCharCode(e.keyCode).toLowerCase();
        } else {
          stemp = null;
        }
      }
      input.scrollIntoView(true);
    }, false);

    addCommand('clear', 'clear terminal', function () {
      output.innerHTML = '';
      input.value = '';
    });

    addCommand('date', 'show current date', function () {
      write([ (new Date()).toLocaleString(), '<br>' ]);
    });

    addCommand('monitor', 'monitor activity', function () {
      MONITORING = true;
      write([ 'monitoring is on<br>' ]);
    });

    addCommand('nomonitor', 'stop monitoring activity', function () {
      MONITORING = false;
      write([ 'monitoring is not on<br>' ]);
    });

    addCommand('start', 'start deadSwap', function () {
      if (RUNNING) {
        write([ 'deadSwap is activated<br>' ]);
      } else if (typeof SMS === 'undefined') {
        write([ 'device unable to send sms<br>' ]);
      } else {
        SMS.startWatch(function () {
          document.addEventListener('onSMSArrive', onSMS);
          SMS.enableIntercept(true);
          RUNNING = true;
          write([ '<p>~# DEADSWAP ACTIVATED @~~/~</p>' ]);
          write([ 'stop to deactivate<br>' ]);
          write([ 'nomonitor to stop watching activity<br>' ]);
          cordova.plugins.notification.badge.set(MESSAGES);
        }, function () {
          write([ 'failed to intercept sms<br>' ]);
        });
      }
    });

    addCommand('stop', 'stop deadSwap', function () {
      if (!RUNNING) {
        write([ 'deadSwap is deactivated<br>' ]);
      } else if (typeof SMS === 'undefined') {
        write([ 'device unable to send sms<br>' ]);
      } else {
        SMS.stopWatch(function () {
          SMS.enableIntercept(false);
          document.removeEventListener('onSMSArrive', onSMS);
          RUNNING = false;
          write([ '<p>~# DEADSWAP DEACTIVATED @~~/~</p>' ]);
          write([ 'start to activate<br>' ]);
          cordova.plugins.notification.badge.clear();
        }, function () {
          write([ 'failed to stop watching<br>' ]);
        });
      }
    });

    addCommand('testsms', 'send test sms', function (args) {
      if (args.length === 0) {
        write([ 'usage: testsms [NUMBER]<br>' ]);
        write([ 'i.e. testsms +14169671111<br>' ]);
      } else {
        sendMessage(args[0], 'DΣªD$wâp ┬ës┼ MΣ$$αGè');
      }
    });

    addCommand('inject', 'inject sms', function (args) {
      var msg = 'hej';
      if (args.length === 0) {
        write([ 'usage: inject [NUMBER] [MESSAGE]<br>' ]);
        write([ 'i.e. inject +14169671111 hej<br>' ]);
      } else {
        var number = args[0];
        if (args.length > 1) { 
          args.splice(0,1);
          msg = args.join(" "); 
        }
        newMessage(number, msg);
        write([ 'injected ', number, ' ', msg, '<br>' ]);
      }
    });

    addCommand('reset', 'delete all data', function () {
      resetState();
      write([ 'all data deleted<br>' ]);
    });

    addCommand('agents', 'list agents', function () {
      for (var agent in AGENTS) {
        write([ agent, ' ', ASSIGNMENTS[agent] || '', '<br>' ]);
      }
    });

    addCommand('sleepers', 'list sleepers', function () {
      write([ SLEEPERS.join('<br>') ]);
    });

    write([ '<div>Welcome to ', document.title, '! (v', VERSION, ')</div>' ]);
    run('date');

    write([ '<p>Documentation: type "help"</p>' ]);

    var sendMessage = function (number, message) {
      if (typeof SMS === 'undefined') {
        write([ 'device unable to send sms to ', number, '<br>' ]);
      } else {
        SMS.sendSMS(number, message, function () {
          monitor([ 'sent message to ', number, '<br>' ]);
        }, function () {
          monitor([ 'failed to send message to ', number, '<br>' ]);
        });
      }
    };

    var addAgent = function (number, message) {
      AGENTS[number] = [];
      monitor([ 'new agent ', number, '<br>' ]);
      sendMessage(number, message || 'ready');
    };

    var queueSleeper = function (number, message) {
      if (SLEEPERS.indexOf(number) === -1) {
        SLEEPERS.push(number);
        sendMessage(number, message || 'welcome');
        monitor([ 'new sleeper ', number, '<br>']);
      }
      if (WAITING.length > 0) {
        var agent = WAITING.shift();
        assignRabbit(agent);
        deliverAgentMessages(agent);
      }
    };

    var assignRabbit = function (agent) {
      var rabbit = SLEEPERS.shift();
      ASSIGNMENTS[agent] = rabbit;
      RABBITS[rabbit] = agent;
      monitor([ 'new rabbit ', rabbit, ' for agent ', agent, '<br>' ]);
    };

    var promoteRabbit = function (rabbit) {
      var agent = RABBITS[rabbit];
      delete RABBITS[rabbit];
      delete AGENTS[agent];
      delete ASSIGNMENTS[agent];
      addAgent(rabbit);
      queueSleeper(agent, 'ok');
      monitor([ 'rabbit ', rabbit, ' promoted<br>']);
    };

    var quitRabbit = function (rabbit) {
      var agent = RABBITS[rabbit];
      delete RABBITS[rabbit];
      delete ASSIGNMENTS[agent];
      addAgent(agent, 'quit');
      queueSleeper(rabbit, 'quit');
      monitor([ 'rabbit ', rabbit, ' quit<br>']);
    };

    var deliverAgentMessages = function (agent) {
      if (ASSIGNMENTS[agent]) {
        AGENTS[agent].forEach(function (msg) {
          sendMessage(ASSIGNMENTS[agent], msg);
        });
        AGENTS[agent] = [];
      }
    };

    var agentMessage = function (agent, message) {
      AGENTS[agent].push(message);
      if (!ASSIGNMENTS[agent]) {
        if (SLEEPERS.length > 0) {
          assignRabbit(agent);
        } else {
          WAITING.push(agent);
        }
      }
      deliverAgentMessages(agent);
    };

    var rabbitMessage = function (rabbit, message) {
      sendMessage(RABBITS[rabbit], message);
    };

    var goodbye = function (number) {
      monitor([ 'goodbye ', number, '<br>' ]);
      if (AGENTS[number]) {
        sendMessage(number, 'sorry, not now');
      } else if (RABBITS[number]) {
        quitRabbit(number);
      } else {
        var i = SLEEPERS.indexOf(number);
        if (i > -1) { SLEEPERS.splice(i, 1); }
      }
    };

    var onSMS = function (e) {
      if (e.data.address.length > 8) {
        newMessage(e.data.address, e.data.body);
      }
    };

    var monitor = function (strings) {
      if (MONITORING) { write(strings); }
    };

    var saveState = function () {
      localforage.setItem('state', JSON.stringify({
        messages: MESSAGES,
        agents: AGENTS,
        assignments: ASSIGNMENTS,
        rabbits: RABBITS,
        waiting: WAITING,
        sleepers: SLEEPERS
      }));
    };

    var resetState = function () {
      MESSAGES = 0;
      AGENTS = {};
      ASSIGNMENTS = {};
      RABBITS = {};
      WAITING = [];
      SLEEPERS = [];
      saveState();
    };

    var RUNNING = false;
    var MONITORING = true;

    var state = JSON.parse(storage) || {};

    // MESSAGES RECIEVED
    var MESSAGES = state.messages || 0;

    // messages from agents
    var AGENTS = state.agents || {};

    // rabbits assigned to agents
    var ASSIGNMENTS = state.assignments || {};

    // agents rabbits are assigned to 
    var RABBITS = state.rabbits || {};

    // agent messages waiting for new sleepers
    var WAITING = state.waiting || [];
    // sleepers waiting for agents messages
    var SLEEPERS = state.sleepers || [];

    var newMessage = function (number, message) {
      monitor([ 'message recieved from ', number, '<br>']);
      if ('ping' === message.trim().toLowerCase().substr(0,4)) {
        sendMessage(number, "here");
      } 

      if ('goodbye' === message.trim().toLowerCase()) {
        goodbye(number);
      } else if (AGENTS[number]) {
        if ('ok' === message.trim().toLowerCase()) {
          if (ASSIGNMENTS[number]) { promoteRabbit(ASSIGNMENTS[number]); }
        } else if ('bye' === message.trim().toLowerCase()) {
          if (ASSIGNMENTS[number]) { quitRabbit(ASSIGNMENTS[number]); }
        } else {
          agentMessage(number, message);
        }
      } else if (RABBITS[number]) {
        if ('ok' === message.trim().toLowerCase()) {
          promoteRabbit(number);
        } else if ('bye' === message.trim().toLowerCase()) {
          quitRabbit(number);
        } else {
          rabbitMessage(number, message);
        }
      } else {
        if ('got one!' === message.trim().toLowerCase()) {
          var i = SLEEPERS.indexOf(number);
          if (i > -1) { SLEEPERS.splice(i, 1); }
          addAgent(number);
        } else {
          queueSleeper(number);
        }
      }
      MESSAGES = MESSAGES + 1;
      cordova.plugins.notification.badge.set(MESSAGES);
      saveState();
    };
  });
})();

/* deadSwap by Telekommunisten */
// vim: tabstop=2 shiftwidth=2 expandtab
